
# react-native-unique-device-id

## Getting started

`$ npm install https://tructmkst@bitbucket.org/tructmkst/uniquedeviceid.git --save`

### Mostly automatic installation

`$ react-native link react-native-unique-device-id`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-unique-device-id` and add `RNUniqueDeviceId.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNUniqueDeviceId.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNUniqueDeviceIdPackage;` to the imports at the top of the file
  - Add `new RNUniqueDeviceIdPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-unique-device-id'
  	project(':react-native-unique-device-id').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-unique-device-id/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-unique-device-id')
  	```


## Usage
```javascript
import RNUniqueDeviceId from 'react-native-unique-device-id';


RNUniqueDeviceId.getUUID().then((uuid) =>
{
  console.log('UUID here: ');
  console.log(uuid);

});

    
RNUniqueDeviceId;
```
  