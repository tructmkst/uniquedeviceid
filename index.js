
import { NativeModules } from 'react-native';

const { RNUniqueDeviceId } = NativeModules;

export default RNUniqueDeviceId;
